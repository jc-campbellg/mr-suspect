{
    "id": "efb1dbc6-8613-45ac-b212-59967ea7eaf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_snake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "140ca78b-6eeb-458e-a711-7c194fe2a978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efb1dbc6-8613-45ac-b212-59967ea7eaf5",
            "compositeImage": {
                "id": "a892edeb-e4a2-4bd4-ad2e-5f8222481bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "140ca78b-6eeb-458e-a711-7c194fe2a978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11248921-17da-46b2-a981-d6be260a3e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "140ca78b-6eeb-458e-a711-7c194fe2a978",
                    "LayerId": "18632699-556f-4131-8b03-448901ef91bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "18632699-556f-4131-8b03-448901ef91bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efb1dbc6-8613-45ac-b212-59967ea7eaf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}