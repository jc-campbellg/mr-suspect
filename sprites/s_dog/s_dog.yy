{
    "id": "471d0a71-01ff-4b03-bd7d-6c4be061887b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "235ac565-d941-4d7a-bae6-9e9a7a4c69d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "471d0a71-01ff-4b03-bd7d-6c4be061887b",
            "compositeImage": {
                "id": "5c40110b-79d8-4f2b-a767-c2734cdf9840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235ac565-d941-4d7a-bae6-9e9a7a4c69d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e96fb1-b1cc-4643-8d0f-ee8891bd75cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235ac565-d941-4d7a-bae6-9e9a7a4c69d9",
                    "LayerId": "406ef086-6824-4039-bdee-61c1ce931f89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "406ef086-6824-4039-bdee-61c1ce931f89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "471d0a71-01ff-4b03-bd7d-6c4be061887b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}