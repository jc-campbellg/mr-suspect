{
    "id": "1136813f-a90f-4c2f-8556-5576f82febe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_server",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb8f0cb5-fae6-4632-8175-a9d6181d514c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1136813f-a90f-4c2f-8556-5576f82febe7",
            "compositeImage": {
                "id": "d29406e8-c023-4e6d-b842-46a93e56bbf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8f0cb5-fae6-4632-8175-a9d6181d514c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bb52b9-f67c-481c-82f8-5e3cbcdf58bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8f0cb5-fae6-4632-8175-a9d6181d514c",
                    "LayerId": "9f52589c-950b-4485-8af9-3236db44cde3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "9f52589c-950b-4485-8af9-3236db44cde3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1136813f-a90f-4c2f-8556-5576f82febe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}