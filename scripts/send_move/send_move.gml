var clientBuffer = o_client.clientBuffer;
var client = o_client.client;

buffer_seek(clientBuffer, buffer_seek_start, 0);
buffer_write(clientBuffer, buffer_u8, net.move);
buffer_write(clientBuffer, buffer_u16, argument0);
buffer_write(clientBuffer, buffer_u16, argument1);
network_send_packet(client, clientBuffer, buffer_tell(clientBuffer));