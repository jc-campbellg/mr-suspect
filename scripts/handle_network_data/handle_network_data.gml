var buffer = argument0;
var msgId = buffer_read(buffer, buffer_u8);
var p = o_player;

switch(msgId) {
	case net.join:
		var socket = buffer_read(buffer, buffer_u8);
		var sx = buffer_read(buffer, buffer_u16);
		var sy = buffer_read(buffer, buffer_u16);
		var inst = instance_create_depth(sx, sy, depth, o_player);
		// save socket for references
		inst.socketId = socket;
		break;
		
	case net.ping:
		var time = buffer_read(buffer, buffer_s32);
		ping = current_time - time;
		send_ping();
		break;
		
	case net.move:
		var nx = buffer_read(buffer, buffer_u16);
		var ny = buffer_read(buffer, buffer_u16);
		p.x = nx;
		p.y = ny;
		break;
}