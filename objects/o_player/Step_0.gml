/// @description Get Keymaps
keyDown = keyboard_check(vk_down);
keyUp = keyboard_check(vk_up);
keyLeft = keyboard_check(vk_left);
keyRight = keyboard_check(vk_right);

var anyPress = keyDown || keyUp || keyLeft || keyRight;

var nx = x;
var ny = y;

if (keyDown) {
	ny = y + moveSpeed;
}

if (keyUp) {
	ny = y - moveSpeed;
}

if (keyLeft) {
	nx = x - moveSpeed;
}

if (keyRight) {
	nx = x + moveSpeed;
}

if (anyPress) {
	send_move(nx, ny);
}