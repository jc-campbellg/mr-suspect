/// @description Connect client to server
enum net {
	join,
	ping,
	move,
	moveSync
};

client = network_create_socket(network_socket_tcp);
isOnline = network_connect(client, "127.0.0.1", 6510);

clientBuffer = buffer_create(2048, buffer_fixed, 1);

ping = 0;

if (isOnline >= 0) {
	send_ping();
	send_join();
}