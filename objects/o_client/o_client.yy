{
    "id": "a451aa61-49ec-45d5-88bb-dda1f83386f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_client",
    "eventList": [
        {
            "id": "aec6dd86-7495-4862-8e65-67d31ba7f74c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a451aa61-49ec-45d5-88bb-dda1f83386f3"
        },
        {
            "id": "1f92cde8-31fd-4829-97e5-85674736fb4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "a451aa61-49ec-45d5-88bb-dda1f83386f3"
        },
        {
            "id": "fab0a782-0f54-43a6-b8bd-ba43c3733670",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a451aa61-49ec-45d5-88bb-dda1f83386f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1136813f-a90f-4c2f-8556-5576f82febe7",
    "visible": true
}