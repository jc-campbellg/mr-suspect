/// @description Online Gaming
var typeEvent = async_load[? "type"];

switch(typeEvent) {
	case network_type_data:
		var buffer = async_load[? "buffer"];
		buffer_seek(buffer, buffer_seek_start, 0);
		handle_network_data(buffer);
		break;
}